const fs = require('fs')
const {ncp} = require('ncp')
const rimraf = require('rimraf')

const from = 'build/static'
const to = 'www/static'
const appName = 'nrg-uploader'
const favicon = {
  from: 'build/favicon.ico',
  to: 'www/favicon.ico'
}

rimraf.sync(to)

fs.copyFile(favicon.from, favicon.to, (error) => {
  if (error) {
    return console.error(error)
  }
})

ncp(from, to, {
  transform: (read, write) => {
    if (write.path.match(/main\.(.*)\./g)) {
      read.pipe(fs.createWriteStream(write.path.replace(/main\.(.*)\./g, `${appName}.`)))
    } else {
      read.pipe(write)
    }
  },
  filter: (fileName) => !fileName.match(/\.map$/g),
}, (error) => {
  if (error) {
    return console.error(error)
  }
})