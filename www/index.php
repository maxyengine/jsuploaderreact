<?php
if (!empty($_GET['r'])) {
    require __DIR__.'/server/run.php';
    exit(0);
}
?><!doctype html>
<html lang="en">
<head>
	<title>File Uploader</title>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no"/>
	<link rel="shortcut icon" href="docs/assets/favicon.ico"/>
	<link href="./static/css/nrg-uploader.css" rel="stylesheet">
	<style type="text/css">
		* {
			padding: 0;
			margin: 0;
		}

		body {
			background: #2c3438;
			padding-top: 50px;
		}

		#uploader {
			margin: 0 auto;
			min-width: 320px;
			max-width: 728px;
		}
	</style>
</head>
<body>

<div id="uploader"></div>

<script src="./static/js/nrg-uploader.js"></script>
<script language="JavaScript">
  var uploader = new $nrg.Uploader({
    apiUrl: '.',
    wrapper: document.getElementById('uploader')
  })

  uploader.run()
</script>
</body>
</html>